<?php
namespace Cadence\Movie\Helper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Config
{
    const MOVIE_ATTRIBUTE_SET_ID = '9';
    const MOVIE_CATEGORY_ID = '3';
    const CONFIG_API_URL = 'cadencemovieapi/general/api_url';

    const CONFIG_API_KEY = 'cadencemovieapi/general/api_key';
    
    protected $scopeConfig;
 
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ){
        $this->scopeConfig = $scopeConfig;
    }

    public function curlHit($url){
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'GET',
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $data = json_decode($response);
        return $data;
    }
    /**
     * Return api url 
     * @return string
     */
    public function getApiUrl() {
        return $this->scopeConfig->getValue(
            self::CONFIG_API_URL,ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Return Api key 
     * @return string
     */
    public function getApiKey() {
        return $this->scopeConfig->getValue(
            self::CONFIG_API_KEY,ScopeInterface::SCOPE_STORE
        );
    }


}

