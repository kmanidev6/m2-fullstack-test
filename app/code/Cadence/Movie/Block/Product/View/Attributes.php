<?php

namespace Cadence\Movie\Block\Product\View;

class Attributes extends \Magento\Framework\View\Element\Template
{
    protected $registry;
    protected $_entityAttribute;
    protected $_attributeOptionCollection;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Eav\Model\Entity\Attribute $entityAttribute,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\Collection $attributeOptionCollection,
        \Magento\Framework\Registry $registry,
        array $data = []
    ){ 
        $this->_entityAttribute = $entityAttribute;
        $this->_attributeOptionCollection = $attributeOptionCollection;
        $this->_registry = $registry;
        parent::__construct($context, $data);
    }
    /**
     * Return attribute data 
     * @return array
     */
    public function getAttributeData($entityType, $attributeCode) 
    {
        $attributeData = $this->_entityAttribute
                    ->loadByCode($entityType, $attributeCode);
        return $attributeData;
    }
     /**
     * Return attribute options 
     * @return array
     */
    public function getAttributeOptions($attributeId) 
    {
        $attributeOptions = $this->_attributeOptionCollection
                    ->setPositionOrder('asc')
                    ->setAttributeFilter($attributeId)
                    ->setStoreFilter()
                    ->load();
        return $attributeOptions;
    }
    public function _prepareLayout(){
        return parent::_prepareLayout();
    }

     /**
     * Return Genre
     * @return String
     */
    public function getcustomGenre()
    {
        return $this->getCurrentProduct()->getGenre();
    }

    /**
     * Return Year
     * @return String
     */
     public function getcustomYear()
    {
        return $this->getCurrentProduct()->getYear();
    }

    /**
     * Return Actors
     * @return String
     */
     public function getcustomActors()
    {
         return $this->getCurrentProduct()->getActors();
    }

    /**
     * Return VoteAverage
     * @return String
     */
     public function getcustomVoteAverage()
    {
        return $this->getCurrentProduct()->getVoteAverage();
    }

     /**
     * Return Director
     * @return String
     */
    public function getcustomDirector()
    {
       return $this->getCurrentProduct()->getDirector();
    }

     /**
     * Return Producer
     * @return String
     */
    public function getcustomProducer()
    {
       return $this->getCurrentProduct()->getProducer();
    }

     /**
     * Return Current Product
     * @return String
     */
    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');
    }
}