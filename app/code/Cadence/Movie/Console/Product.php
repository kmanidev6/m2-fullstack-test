<?php
namespace Cadence\Movie\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Product extends Command
{   
    const PRODUCT_PRICE = 5.99;
    const PRODUCT_QTY = 100;
    /** @var \Cadence\Movie\Helper\Config **/
    protected $_helper;

    /** @var \Magento\Catalog\Model\Product  **/
    protected $_product;

    /** @var \Magento\Catalog\Model\Product  **/
    protected $product;  

     /** @var \Magento\Framework\App\State **/
    private $state;

    public function __construct(
          \Cadence\Movie\Helper\Config $helper,
          \Magento\Catalog\Api\Data\ProductInterfaceFactory $productFactory,
          \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
          \Magento\Framework\View\Result\PageFactory $pageFactory,
          \Magento\Catalog\Model\ProductFactory $_productloader,
          \Magento\Catalog\Model\Product $product,
          \Magento\Framework\App\State $state
    ) {
          $this->state = $state;  
          $this->_helper = $helper;
          $this->productFactory = $productFactory;
          $this->productRepository = $productRepository;
          $this->_pageFactory = $pageFactory;
          $this->_productloader = $_productloader;
          $this->_product = $product;
          parent::__construct('create:product');
    }

    protected function configure()
    {
       $this->setName('create:product');
       $this->setDescription('Create Product command line');
       
       parent::configure();
    }

   protected function execute(InputInterface $input, OutputInterface $output)
   {    
        echo "Script excutation start";
        $apiUrl = $this->_helper->getApiUrl();
        $apiKey = $this->_helper->getApiKey();
        $popularMoviesUrl  = $apiUrl.'popular?api_key='.$apiKey;
        $popularMovies = $this->_helper->curlHit($popularMoviesUrl);
        $categoryId = $this->_helper::MOVIE_CATEGORY_ID;
        $attributeId = $this->_helper::MOVIE_ATTRIBUTE_SET_ID;
        $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_FRONTEND);
        if(count($popularMovies->results)>0)
        {
            foreach($popularMovies->results as $value){
                $producerName = [];
                $directorName = [];
                $sku = $value->id;
                $skuNum = strval($sku);
                $name = $value->title;
                $description = $value->overview;
                $movieDetailsUrl = $apiUrl.$skuNum.'?api_key='.$apiKey;
                $movieDetails = $this->_helper->curlhit($movieDetailsUrl);
                $voteAverage = $movieDetails->vote_average;
                $genres = array_column($movieDetails->genres, 'name');
                $genresName =   implode(',', $genres);
                $releaseDate = date_create($movieDetails->release_date);
                $year = date_format($releaseDate, 'Y');
                $creditsDetails = $apiUrl.$skuNum.'/credits?api_key='.$apiKey;
                $creditResponse = $this->_helper->curlhit($creditsDetails);
                $castName = array_column($creditResponse->cast, 'name');
                $actors =   implode(',', $castName);
                $crewJob = $creditResponse->crew;
                foreach($crewJob as $crew){
                    $crewJobName =  $crew->job;
                    if ($crewJobName == 'Producer') {
                        $producerName[] = $crew->name;
                    }
                    if ($crewJobName == 'Director') {     
                        $directorName[] = $crew->name;
                    }
                }
                $producer =  implode(',', $producerName);
                $director = implode(',', $directorName);
                if($this->_product->getIdBySku($skuNum)) {
                    echo 'This Product is already exist';
                }else{
                    try {

                        // Create product factory
                        $product = $this->productFactory->create();
                        $product->setSku($skuNum);
                        $product->setName($name);
                        $product->setDescription($description);
                        $product->setWebsiteIds([1]);
                        $product->setCategoryIds($categoryId);
                        $product->setTypeId(\Magento\Catalog\Model\Product\Type::TYPE_VIRTUAL);
                        $product->setVisibility(\Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH);
                        // To make product visible in both catalog,search
                        $product->setPrice(self::PRODUCT_PRICE);
                        $product->setAttributeSetId($attributeId); // Attribute set for products
                        $product->setVoteAverage($voteAverage);
                        $product->setGenre($genresName);
                        $product->setActors($actors);
                        $product->setProducer($producer);
                        $product->setDirector($director);
                        $product->setYear($year);
                        $product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);
                        $product->setStockData(array(
                                        'use_config_manage_stock' => 0,
                                        'manage_stock' => 1,
                                        'is_in_stock' => 1,
                                        'qty' => self::PRODUCT_QTY
                                    ));
                        $product = $this->productRepository->save($product);
                        $product->save();
                    } catch (Exception $e){
                         echo $e->getMessage();
                    } 
                }  
            }
        }
        else
        {
            echo "Record not found";
        }

        echo "Script excuted";
    }
}


